import tkinter as tk
from PIL import Image, ImageTk
import cv2
import imutils

ventana = tk.Tk()
ventana.geometry("1000x680+200+10")
ventana.title("captura de video")
ventana.resizable(width=False, height=False)
ventana.config(bg="grey")


#Funciones video
video = None

def video_stream():
    global video
    video = cv2.VideoCapture(0)
    iniciar()


def iniciar():
    global video
    ret, frame = video.read()
    if ret == True:
        etiq_de_video.place(x=187, y=75)
        frame = imutils.resize(frame, width=640)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(frame)
        image = ImageTk.PhotoImage(image=img)
        etiq_de_video.config(image=image)
        etiq_de_video.image = image
        etiq_de_video.after(10, iniciar)
    

def quitar():
    global video
    etiq_de_video.place_forget()
    video.release()










#Botones
boton = tk.Button(ventana, text="Iniciar video", bg="pink", relief="flat", 
                  cursor="hand2", command=video_stream, width=15, height=2, font=("Calisto MT", 12, "bold"))

boton.place(x=200, y=590)


boton2 = tk.Button(ventana, text="Quitar video", bg="pink", relief="flat", 
                  cursor="hand2", command=quitar, width=15, height=2, font=("Calisto MT", 12, "bold"))

boton2.place(x=640, y=590)




#Etiquetas
etiq_de_video = tk.Label(ventana, bg="black")
etiq_de_video.place(x=187, y=75)


ventana.mainloop()
