import tkinter as tk

def actualizar_etiqueta():
    valor = caja_texto.get()
    etiqueta.config(text=valor)

# Crear la ventana principal
ventana = tk.Tk()

# Crear la variable de control
valor_texto = tk.StringVar()

# Crear la caja de texto y asociarla con la variable de control
caja_texto = tk.Entry(ventana, textvariable=valor_texto)
caja_texto.pack()

# Crear la etiqueta y mostrar el valor inicial de la variable de control (en este caso, vacío)
etiqueta = tk.Label(ventana, text="")
etiqueta.pack()

# Crear el botón que actualizará la etiqueta con el valor introducido en la caja de texto
boton = tk.Button(ventana, text="Actualizar etiqueta", command=actualizar_etiqueta)
boton.pack()

# Mostrar la ventana
ventana.mainloop()
